###########
# Stage one 
###########

# fetches and builds the 'ws' tool. It's a command line tool to debug web sockets. We will use this to run healthchecks

FROM golang:1.11-stretch as wsbuild


### Update System + install git
# set -x: make shell print commands while executing them
# rm -rf /var/lib/apt/lists/*: Clear Caches. Always do this after installing something with the package manager.
RUN set -x && \
    apt-get update && \
    apt-get install -yq --no-install-recommends \
        git \
    && rm -rf /var/lib/apt/lists/*

# download and install ws
RUN go get github.com/hashrocket/ws && \
    go install github.com/hashrocket/ws



############################
# Stage two, final container 
############################

# Start with NodeJS 8 on Debian "Stretch"
FROM node:8-stretch

MAINTAINER Johannes Wuensch <j.wuensch@protonmail.com>

# Copy built 'ws' tool from previous run - needed for healthchecks
COPY --from=wsbuild /go/bin/ws /bin/ws

# copy source code of taiga events into the container
COPY taiga-events /usr/src/taiga-events

WORKDIR /usr/src/taiga-events

# Install the javascript dependencies and install the coffeescript interpreter globally.
RUN npm install && npm install -g coffee-script

# add the custom config file which will be edited by the docker-entrypoint.sh script on container startup
COPY events-config.json /usr/src/taiga-events/config.json

# some environment variables that have to exist to generate a valid config.json. Those are ment to be adjusted via docker-compose or swarm on startup, not here
ENV EVENT_HOST rabbit
ENV EVENT_VIRTUAL_HOST taiga
ENV EVENT_USERNAME taiga
ENV EVENT_PASSWORD "!replace-me!"
ENV TAIGA_SECRET_KEY "!!!REPLACE-ME-j1475u1J^U*(y2fgd98u51u5981urf98u2o5uvoiiuzhlit3)!!!"
ENV EVENT_SERVER_PORT 8888
ENV EVENT_RABBIT_PORT 5672

# add entrypoint and healthcheck scripts
COPY docker-entrypoint.sh /docker-entrypoint.sh
COPY docker-healthcheck.sh /usr/local/bin/

# Define healthcheck with a custom shell script (we don't need a path for the healthcheck file as we copied it to the /usr/local/bin/ directory before)
# Don't use it for now as the script "docker-healthcheck.sh" will create zombie processes and spam your server until you get an "no space left on device" error.
#HEALTHCHECK --interval=15s --timeout=3s --retries=3 CMD ["docker-healthcheck.sh"]

# Run entrypoint script that replaces some variables in config.json with the value of some environment variables on container startup.
ENTRYPOINT ["/docker-entrypoint.sh"]

# finally run the taiga events service
CMD ["coffee", "index.coffee"]