#!/bin/sh

# IMPORTANT! DON'T USE THIS HEALTHCHECK IN IT'S CURRENT FORM. IT WILL PRODUCE 3 ZOMBIE PROCESSES EACH TIME IT IS CALLED! THIS WILL WORK SOME TIME UNTIL THE MAXIMUM NUMBER OF PROCESSES IS REACHED FOR YOUR SYSTEM. AFTER THAT YOU WILL GET AN "No space left on device" ERROR FOR EACH COMMAND YOU TRY TO RUN ON YOUR SYSTEM


# This healthcheck requires ws (github.com/hashrocket/ws) to be installed. It's a command line tool to debug web sockets.

# 2>&1: redirecting stderr (file descriptor 2) to stdout (file descriptor 1)
# &> /dev/null: redirecting result to /dev/null which is a null device file that immediately discards all data written to it. It is faster then outputting to screen. It still returns 0 if writing to it would have been successful or a number with the error code.
# $?: returns value of the last executed command. 0 for success, any number for an error.

(echo '{"cmd":"ping"}' ; sleep 1 ; ) | ws ws://localhost:EVENT_SERVER_PORT/events 2>&1 | grep '{"cmd":"pong"}' &> /dev/null

if [ "$?" = "0" ]; then

  echo "healthy"
  exit 0;

else

  echo "unhealthy"
  exit 1;

fi