#!/bin/bash

echo "Configuring config.json by replacing placeholders with environment variable values."

# Automatically replace placeholders in config.json with the values of the environment variables. Make sure those environment variables has been set, otherwise the resulting config.json will be erroneous and Taiga will not work
sed -i "s/EVENT_HOST/$EVENT_HOST/g" config.json
sed -i "s/EVENT_VIRTUAL_HOST/$EVENT_VIRTUAL_HOST/g" config.json
sed -i "s/EVENT_USERNAME/$EVENT_USERNAME/g" config.json
sed -i "s/EVENT_PASSWORD/$EVENT_PASSWORD/g" config.json
sed -i "s/TAIGA_SECRET_KEY/$TAIGA_SECRET_KEY/g" config.json
sed -i "s/EVENT_SERVER_PORT/$EVENT_SERVER_PORT/g" config.json
sed -i "s/EVENT_RABBIT_PORT/$EVENT_RABBIT_PORT/g" config.json

echo "Configuring config.json finished."


echo "Configuring link for healthcheck"

# replace placeholder in docker-healthcheck.sh
sed -i "s/EVENT_SERVER_PORT/$EVENT_SERVER_PORT/g" /usr/local/bin/docker-healthcheck.sh

echo "Healthcheck updated"


# Continue with the CMD command of the dockerfile.
exec "$@"